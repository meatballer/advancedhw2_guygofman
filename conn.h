#pragma once

#pragma warning (disable:4996)

#include <winsock2.h>
#include <windows.h>

#define VALIDATE_CONN(e, t, r) if (e) { printf("%s%d\n", t, WSAGetLastError()); WSACleanup(); return r; }
#define VALIDATE_CONN_SOCKET(e, t, r) if (e) { printf("%s%d\n", t, WSAGetLastError()); closesocket(soc); WSACleanup(); return r; }
#define VALID_RET(e, t, r) if (e) { printf("%s\n", t); return r;}
#define BUFLEN 1024

int createSocket();
int connectAndListen(SOCKET* clientSoc, char* _ip);
int closeconn(SOCKET _activeSoc);