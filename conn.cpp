#include <stdio.h>
#include <string.h>
#include <ws2tcpip.h>
#include "conn.h"

static SOCKET soc;

int createSocket()
{
	addrinfo *result = NULL, *ptr = NULL, hints;
	ZeroMemory(&hints, sizeof (hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	VALIDATE_CONN(getaddrinfo(NULL, "7777", &hints, &result), "Configuring socket failed", -1);
	soc = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	VALIDATE_CONN(soc == INVALID_SOCKET, "socket creation failed!: ", -2);
	VALIDATE_CONN_SOCKET(bind(soc, result->ai_addr, (int)result->ai_addrlen) == SOCKET_ERROR, "Error binding socket: ", -3);
	freeaddrinfo(result);
	VALIDATE_CONN_SOCKET(listen(soc, SOMAXCONN) == SOCKET_ERROR, "Error listening: ", -4);

	return 0;
}

int connectAndListen(SOCKET* clientSoc, char* _ip)
{
	sockaddr_in peerAddr = { 0 };
	int size = sizeof(peerAddr);

	*clientSoc = accept(soc, (sockaddr*)&peerAddr, &size);
	VALIDATE_CONN_SOCKET(*clientSoc == INVALID_SOCKET, "Error accepting connection: ", -5);

	return 0;
}

int closeconn(SOCKET _activeSoc)
{
	shutdown(_activeSoc, SD_SEND);
	return 0;
}