#include <stdio.h>
#include <thread>
#include <vector>
#include "conn.h"

static std::vector<std::thread> clients;

int handleClient(SOCKET clientSoc)
{
	send(clientSoc, "Accepted", BUFLEN, 0);
	closeconn(clientSoc);
	return 0;
}

int main()
{
	char ip[20] = "";
	SOCKET clientSoc;
	WSADATA info;
	WSAStartup(MAKEWORD(2, 0), &info);
	VALID_RET(createSocket(), "", -1);

	while (true)
	{
		connectAndListen(&clientSoc, ip);
		clients.push_back(std::thread(handleClient, clientSoc));
	}
	closesocket(clientSoc);
	WSACleanup();
	return 0;
}